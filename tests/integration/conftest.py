from unittest import mock

import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="idem")

    with mock.patch("sys.argv", ["idem"]):
        hub.pop.config.load(["idem", "acct"], cli="idem")

    yield hub
