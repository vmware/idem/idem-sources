def test_gather_default(hub, idem_cli):
    """
    With no acct_profile specified, the default becomes the current user's private key
    """
    ret = idem_cli("exec", "ctx.acct", "--output=json")
    assert not ret.retcode, ret.stderr
    assert ret.json.result, ret.json.comment
    assert ret.json.ret
