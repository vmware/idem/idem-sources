def __init__(hub):
    hub.exec.ctx.ACCT = ["git"]


def acct(hub, ctx):
    """
    Return exactly what was passed into ctx.acct
    """
    ret = {
        "result": True,
        "ret": ctx.acct,
        "comment": [],
    }
    if not ctx.acct:
        ret["result"] = False

    return ret
