#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.8/tests.txt requirements/tests.in
#
acct==8.6.2
    # via
    #   idem
    #   idem-sources
    #   pop-evbus
aiofiles==23.2.1
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
cffi==1.16.0
    # via cryptography
colorama==0.4.6
    # via
    #   idem
    #   rend
cryptography==42.0.5
    # via acct
dict-toolbox==4.1.1
    # via
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pop-tree
    #   pytest-pop
    #   rend
docstring-parser==0.15
    # via pop-tree
exceptiongroup==1.2.0
    # via pytest
gitdb==4.0.11
    # via gitpython
gitpython==3.1.42
    # via idem-sources
file:.#egg=idem-sources
    # via -r requirements/tests.in
idem==25.0.1
    # via
    #   idem-sources
    #   pytest-idem
iniconfig==2.0.0
    # via pytest
jinja2==3.1.3
    # via
    #   idem
    #   rend
jmespath==1.0.1
    # via idem
lazy-object-proxy==1.10.0
    # via pop
markupsafe==2.1.5
    # via jinja2
mock==5.1.0
    # via pytest-pop
msgpack==1.0.8
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
nest-asyncio==1.6.0
    # via
    #   pop-loop
    #   pytest-pop
packaging==23.2
    # via pytest
pluggy==1.4.0
    # via pytest
pop-config==12.0.4
    # via
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==7.0.1
    # via idem
pop-loop==1.1.0
    # via
    #   idem
    #   pop
pop-serial==2.0.0
    # via
    #   acct
    #   idem
    #   pop-evbus
pop-tree==12.1.2
    # via idem
pop==27.1.0
    # via
    #   acct
    #   idem
    #   idem-sources
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
pycparser==2.21
    # via cffi
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.18.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-idem==4.0.1
    # via -r requirements/tests.in
pytest-pop==12.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest==8.0.2
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0.1
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   pop-evbus
    #   pytest-idem
    #   rend
rend==7.0.2
    # via
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
smmap==5.0.1
    # via gitdb
sniffio==1.3.1
    # via pop-loop
toml==0.10.2
    # via
    #   idem
    #   rend
tomli==2.0.1
    # via pytest
tqdm==4.66.2
    # via idem
uvloop==0.19.0
    # via idem
wheel==0.42.0
    # via idem
